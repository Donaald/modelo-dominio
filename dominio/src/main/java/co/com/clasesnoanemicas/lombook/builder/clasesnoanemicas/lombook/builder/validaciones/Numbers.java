package co.com.clasesnoanemicas.lombook.builder.clasesnoanemicas.lombook.builder.validaciones;


import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.math.BigInteger;
import java.util.Objects;
import java.util.stream.Stream;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Numbers {


    public static Boolean notNullAndNotEmpty(BigInteger... integers) {
        return Stream.of(integers)
                .anyMatch(Objects::isNull);
    }

    public static Boolean notNegativeOrZero(BigInteger... integers) {
        return Stream.of(integers)
                .anyMatch(integer ->
                        integer.compareTo(BigInteger.ZERO) == 0 ||
                                integer.compareTo(BigInteger.ZERO) == -1);
    }

}
