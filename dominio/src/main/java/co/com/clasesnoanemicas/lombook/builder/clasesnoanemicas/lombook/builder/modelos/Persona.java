package co.com.clasesnoanemicas.lombook.builder.clasesnoanemicas.lombook.builder.modelos;


import co.com.clasesnoanemicas.lombook.builder.clasesnoanemicas.lombook.builder.excepciones.ValorRequerido;
import co.com.clasesnoanemicas.lombook.builder.clasesnoanemicas.lombook.builder.validaciones.Numbers;
import co.com.clasesnoanemicas.lombook.builder.clasesnoanemicas.lombook.builder.validaciones.Strings;

import java.math.BigInteger;


public abstract class Persona {

    private static final String TIPO_REQUERIDO = "El tipo es un valor requerido";
    private static final String CEDULA_REQUERIDO = "La cedula es un valor requerido";
    private static final String NOMBRE_REQUERIDO = "El nombre es un valor requerido";
    private static final String APELLIDO_REQUERIDO = "El apellido es un valor requerido";


    public final String tipo;
    public final BigInteger cedula;
    public final String nombre;
    public final String apellido;


    public Persona(
            String tipo,
            BigInteger cedula,
            String nombre,
            String apellido
    ) {
        if (Strings.notNullAndNotEmpty(tipo)) {
            throw new ValorRequerido(TIPO_REQUERIDO);
        }

        if (Strings.notNullAndNotEmpty(nombre)) {
            throw new ValorRequerido(NOMBRE_REQUERIDO);
        }

        if (Strings.notNullAndNotEmpty(apellido)) {
            throw new ValorRequerido(APELLIDO_REQUERIDO);
        }

        if (Numbers.notNullAndNotEmpty(cedula)) {
            throw new ValorRequerido(CEDULA_REQUERIDO);
        }

        this.tipo = tipo;
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellido = apellido;
    }
}

