package co.com.clasesnoanemicas.lombook.builder.clasesnoanemicas.lombook.builder.excepciones;

public class ValorRequerido extends RuntimeException {

    public ValorRequerido(String message) {
        super(message);
    }
}
