package co.com.clasesnoanemicas.lombook.builder.clasesnoanemicas.lombook.builder.modelos;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.ToString;

@AllArgsConstructor
@Builder(toBuilder = true)
@ToString
public final class Mascota {

    private final String nombre;
    private final String tipo;


}
