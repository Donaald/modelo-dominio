package co.com.clasesnoanemicas.lombook.builder.clasesnoanemicas.lombook.builder.validaciones;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Objects;
import java.util.stream.Stream;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Strings {


    public static Boolean notNullAndNotEmpty(String... strings) {
        return Stream.of(strings)
                .anyMatch(string -> Objects.isNull(string) || string.isEmpty());
    }


}
