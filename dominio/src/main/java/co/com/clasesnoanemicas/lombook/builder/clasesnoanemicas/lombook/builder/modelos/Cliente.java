package co.com.clasesnoanemicas.lombook.builder.clasesnoanemicas.lombook.builder.modelos;


import co.com.clasesnoanemicas.lombook.builder.clasesnoanemicas.lombook.builder.excepciones.ValorRequerido;
import co.com.clasesnoanemicas.lombook.builder.clasesnoanemicas.lombook.builder.validaciones.Lists;
import co.com.clasesnoanemicas.lombook.builder.clasesnoanemicas.lombook.builder.validaciones.Strings;
import lombok.Builder;
import lombok.ToString;

import java.math.BigInteger;
import java.util.List;


@ToString
public final class Cliente extends Persona {

    private static final String IDENTIFICACION_REQUERIDO = "La identificacion es un valor requerido";
    private static final String DIRECCION_REQUERIDO = "La direccion es un valor requerido";
    private static final String NUMERO_CUENTA_REQUERIDO = "El numero de cuenta es un valor requerido";
    private static final String MASCOTA_REQUERIDO = "Al menos debe suministrar los datos de una mascota";


    private final String identificacion;
    private final String direccion;
    private final String numeroCuenta;
    private final List<Mascota> mascotas;


    @Builder(toBuilder = true)
    public Cliente(
            String identificacion,
            String direccion,
            String numeroCuenta,
            List<Mascota> mascotas,
            String tipo,
            BigInteger cedula,
            String nombre,
            String apellido
    ) {

        super(tipo, cedula, nombre, apellido);

        if (Strings.notNullAndNotEmpty(identificacion)) {
            throw new ValorRequerido(IDENTIFICACION_REQUERIDO);
        }

        if (Strings.notNullAndNotEmpty(direccion)) {
            throw new ValorRequerido(DIRECCION_REQUERIDO);
        }

        if (Strings.notNullAndNotEmpty(numeroCuenta)) {
            throw new ValorRequerido(NUMERO_CUENTA_REQUERIDO);
        }

        if(Lists.notNullAndNotEmpty(mascotas)){
            throw new ValorRequerido(MASCOTA_REQUERIDO);
        }


        this.identificacion = identificacion;
        this.direccion = direccion;
        this.numeroCuenta = numeroCuenta;
        this.mascotas = mascotas;
    }


}
