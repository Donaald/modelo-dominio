package co.com.clasesnoanemicas.lombook.builder.clasesnoanemicas.lombook.builder.validaciones;


import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Lists {

    public static Boolean notNullAndNotEmpty(List list) {
        return list.isEmpty() || Objects.isNull(list);
    }


}
