package co.com.clasesnoanemicas.lombook.builder.clasesnoanemicas.lombook.builder;

import co.com.clasesnoanemicas.lombook.builder.clasesnoanemicas.lombook.builder.modelos.Cliente;
import co.com.clasesnoanemicas.lombook.builder.clasesnoanemicas.lombook.builder.modelos.Mascota;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigInteger;
import java.util.Collections;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {

        SpringApplication.run(Application.class, args);

        Mascota mascota = new Mascota(
                "mi perro",
                "perro"
        );

        Cliente cliente = new Cliente(
                "identificacion",
                "direccion",
                "numero de cuenta",
                Collections.singletonList(mascota),
                "CC",
                BigInteger.valueOf(1111111),
                "Nombre",
                "Apellido"
        );

        Cliente cliente1 = Cliente
                .builder()
                .direccion("direccion")
                .identificacion("identificacion")
                .mascotas(
                        Collections.singletonList(
                                Mascota
                                        .builder()
                                        .tipo("perro")
                                        .nombre("mi perro")
                                        .build()
                        )
                )
                .numeroCuenta("numero de cuenta")
                .tipo("CC")
                .nombre("Nombre")
                .cedula(BigInteger.valueOf(1111111))
                .apellido("Apellido")
                .build();


        System.out.println(cliente);
        System.out.println("-------------------------------------------");
        System.out.println(cliente1);


    }

}
